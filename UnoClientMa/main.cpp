#include "wiconnectserver.h"

#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    WiConnectServer w;
    w.show();
    return a.exec();
}
