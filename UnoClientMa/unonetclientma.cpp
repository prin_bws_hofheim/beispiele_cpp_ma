#include "unonetclientma.h"

UnoNetClientMa::UnoNetClientMa()
{
    tcpSocket = new QTcpSocket;
    QObject::connect(tcpSocket, &QTcpSocket::readyRead, this, &UnoNetClientMa::recData);
}

void UnoNetClientMa::connectToServer(QString host, ushort port)
{
    tcpSocket->connectToHost(host, port);
}

void UnoNetClientMa::sendUserName(QString userName)
{
    QString sendString;
    sendString = QString::number(CommandNumber::commandUsername) + ";" + userName;
    qDebug() << "send: " << sendString;
    tcpSocket->write(sendString.toLocal8Bit());
}


void UnoNetClientMa::sendData(int command)
{
    tcpSocket->write(QString::number(command).toLocal8Bit());
}

void UnoNetClientMa::sendData(int command, QString param)
{
    QString sendString;
    sendString = QString::number(command) + ";" + param;
    tcpSocket->write(sendString.toLocal8Bit());
    qDebug() << "send: " << sendString;
}

void UnoNetClientMa::recData()
{
    QString recString;
    QStringList recStringList;
    int recCommand = -1;

    while (tcpSocket->bytesAvailable()) {
        recString = tcpSocket->readLine();
        qDebug() << "rec: " << recString;

        recStringList = recString.split(";");

        if(recStringList.size()>0 && recStringList.size()<2){
            recCommand = recStringList[0].toInt();
            computeRecData(recCommand);
        }
        if(recStringList.size()>=2){
            recCommand = recStringList[0].toInt();
            computeRecData(recCommand, recStringList[1]);
        }    
    }
}

void UnoNetClientMa::computeRecData(int recCommand)
{
    switch (recCommand) {
    case CommandNumber::commandUsername:

        break;
    default:
        break;
    }
}


void UnoNetClientMa::computeRecData(int recCommand, QString recParam)
{
    switch (recCommand) {
    case CommandNumber::commandUsername:

        break;
    default:
        break;
    }
}
