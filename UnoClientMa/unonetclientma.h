#ifndef UNONETCLIENTMA_H
#define UNONETCLIENTMA_H

#include <QTcpSocket>
#include <QObject>
#include "../uno_server/commandnumber.h"

class UnoNetClientMa : public QObject
{
public:
    UnoNetClientMa();
    void sendData(int command, QString param);
    void sendData(int command);
    void connectToServer(QString host, ushort port);
    void sendUserName(QString userName);

private slots:
    void recData();
private:
    QTcpSocket *tcpSocket;
    void computeRecData(int recCommand);
    void computeRecData(int recCommand, QString recParam);
};

#endif // UNONETCLIENTMA_H
