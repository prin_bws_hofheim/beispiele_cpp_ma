#include "wiconnectserver.h"
#include "ui_wiconnectserver.h"

WiConnectServer::WiConnectServer(QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::WiConnectServer)
{
    ui->setupUi(this);
    netClient = new UnoNetClientMa;
    QObject::connect(ui->btnConnectServer, &QPushButton::clicked, this, &WiConnectServer::connectServer);
    QObject::connect(ui->btnSetUser, &QPushButton::clicked, this, &WiConnectServer::setUser);
}

WiConnectServer::~WiConnectServer()
{
    delete ui;
}


void WiConnectServer::connectServer()
{
    QString username;
    QString host;
    ushort port;

    host = ui->leHost->text();
    port = ui->sbPort->value();

    netClient->connectToServer(host, port);
}

void WiConnectServer::setUser()
{
    netClient->sendUserName(ui->leUserName->text());
}
