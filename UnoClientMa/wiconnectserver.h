#ifndef WICONNECTSERVER_H
#define WICONNECTSERVER_H

#include <QWidget>
#include "unonetclientma.h"

QT_BEGIN_NAMESPACE
namespace Ui { class WiConnectServer; }
QT_END_NAMESPACE

class WiConnectServer : public QWidget
{
    Q_OBJECT

public:
    WiConnectServer(QWidget *parent = nullptr);
    ~WiConnectServer();

private slots:
    void connectServer();
    void setUser();

private:
    Ui::WiConnectServer *ui;
    UnoNetClientMa *netClient;
};
#endif // WICONNECTSERVER_H
