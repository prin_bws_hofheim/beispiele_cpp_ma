#include "brett.h"

/**
 * \brief Konstruktor - Init
 *
 * Der konstruktor legt Objekte fuer Spieler an.
 * Bei Steine Platzhalter
 * nullptr = "leer"
 */
Brett::Brett(){

    tempStein = new Stein("Stein1", 3);

    spieler1 = new Spieler(nullptr);
    spieler2 = new Spieler(tempStein);
    for(int i = 0; i < 10; i++)
        steine[i] = nullptr;
}
