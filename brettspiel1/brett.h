#ifndef BRETT_H
#define BRETT_H

#include "spieler.h"
#include "stein.h"

class Brett{
    Spieler* spieler1;
    Spieler* spieler2;
    Stein* steine[10];
    Stein* tempStein;
public:
    Brett(); // Constructor
    // ...
};
#endif // BRETT_H
