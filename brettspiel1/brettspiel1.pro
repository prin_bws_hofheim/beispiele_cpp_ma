TEMPLATE = app
CONFIG += console c++17
CONFIG -= app_bundle

SOURCES += \
        brett.cpp \
        main.cpp \
        spieler.cpp \
        stein.cpp

HEADERS += \
    brett.h \
    spieler.h \
    stein.h
