#include "spieler.h"

Spieler::Spieler(Stein *newStein)
{
    myStein = newStein;
}

QString Spieler::getName()
{
    return name;
}

void Spieler::setName(QString newName)
{
    name = newName;
}

int Spieler::getPunkte()
{
    return punkte;
}

void Spieler::setPunkte(int newPunkte)
{
    punkte = newPunkte;
}
