#include "stein.h"
#include "spieler.h"

Stein::Stein(QString newName, int newPos)
{
    name = newName;
    pos = newPos;
}

QString Stein::getName()
{
    return name;
}

int Stein::getPos()
{
    return pos;
}

void Stein::setPos(int newPos)
{
    pos = newPos;
}

int Stein::getSpielerPunkte()
{
    return mySpieler->getPunkte();
}

Spieler *Stein::getSpieler() const
{
    return mySpieler;
}

void Stein::setSpieler(Spieler *newSpieler)
{
    mySpieler = newSpieler;
}
