#ifndef STEIN_H
#define STEIN_H

#include <QString>

class Spieler;

class Stein
{
public:
    Stein(QString newName, int newPos);
    QString getName();

    int getPos();
    void setPos(int newPos);
    int getSpielerPunkte();

    Spieler *getSpieler() const;
    void setSpieler(Spieler *newSpieler);

protected:
        int pos;

private:
    QString name;
    Spieler *mySpieler;
};

#endif // STEIN_H
