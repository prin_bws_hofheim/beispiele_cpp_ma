#ifndef STEINSPEZIAL1_H
#define STEINSPEZIAL1_H

#include "stein.h"

class SteinSpezial1 : public Stein
{
public:
    SteinSpezial1(QString newName, int newPos);
private:
    int level;
};

#endif // STEINSPEZIAL1_H

#include "stein.h"

