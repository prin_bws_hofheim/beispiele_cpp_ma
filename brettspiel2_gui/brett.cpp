#include "brett.h"

/**
 * \brief Konstruktor - Init
 *
 * Der konstruktor legt Objekte fuer Spieler an.
 * Bei Steine Platzhalter
 * nullptr = "leer"
 */
Brett::Brett(){
    spieler1 = new Spieler();
    for(int i = 0; i < 10; i++)
        steine[i] = nullptr;
}

void Brett::spieler1SetName(QString newName)
{
    spieler1->setName(newName);
}
