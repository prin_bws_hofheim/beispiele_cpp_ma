#ifndef BRETT_H
#define BRETT_H

#include "spieler.h"
#include "stein.h"

class Brett{
    Spieler* spieler1;
    Stein* steine[10];
public:
    Brett(); // Constructor
    void spieler1SetName(QString newName);
};
#endif // BRETT_H
