#ifndef SPIELER_H
#define SPIELER_H

#include <QString>
#include "stein.h"

class Spieler
{
public:
    Spieler();

    QString getName();
    void setName(QString newName);

    int getPunkte();
    void setPunkte(int newPunkte);

private:
    QString name;
    int punkte;

};

#endif // SPIELER_H
