#include "stein.h"

Stein::Stein(QString newName, int newPos)
{
    name = newName;
    pos = newPos;
}

QString Stein::getName()
{
    return name;
}

int Stein::getPos()
{
    return pos;
}

void Stein::setPos(int newPos)
{
    pos = newPos;
}
