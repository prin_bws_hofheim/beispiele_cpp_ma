#ifndef STEIN_H
#define STEIN_H

#include <QString>

class Stein
{
public:
    Stein(QString newName, int newPos);
    QString getName();

    int getPos();
    void setPos(int newPos);

private:
    QString name;
    int pos;
};

#endif // STEIN_H
