#include "widget.h"
#include "ui_widget.h"

Widget::Widget(QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::Widget)
{
    ui->setupUi(this);
    brett = new Brett;
}

Widget::~Widget()
{
    delete ui;
}


void Widget::on_edtSpieler1Name_editingFinished()
{
    brett->spieler1SetName(ui->edtSpieler1Name->text());
}

