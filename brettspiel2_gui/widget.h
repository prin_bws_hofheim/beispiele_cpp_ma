#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>
#include "brett.h"

QT_BEGIN_NAMESPACE
namespace Ui { class Widget; }
QT_END_NAMESPACE

class Widget : public QWidget
{
    Q_OBJECT

public:
    Widget(QWidget *parent = nullptr);
    ~Widget();

private slots:
    void on_edtSpieler1Name_editingFinished();

private:
    Ui::Widget *ui;
    Brett* brett;
};
#endif // WIDGET_H
