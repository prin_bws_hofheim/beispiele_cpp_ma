#include "controller.h"

#include "widget.h"
Controller::Controller(Widget *newGui)
{
    gui = newGui;
    data = new Data;
}

/**
 * @brief neuer Spielstein
 * @param tokenNr Nummer des Steins
 * @param newName Name des Spielers
 */
void Controller::newToken(int tokenNr, QString newName)
{
    data->newToken(tokenNr, newName);
}

int Controller::getPosToken(int tokenNr)
{
    int retValue = -2;
    retValue = data->getPosToken(tokenNr);
    return retValue;
}

/**
 * @brief Stein schieben
 * @param tokenNr Nummer des Steins
 * @param newPosLogic neue Position
 */
void Controller::moveToken(int tokenNr, int newPosLogic)
{
    data->updateToken(tokenNr, newPosLogic);
    gui->logAction("control", "update token " + QString::number(tokenNr));
}
