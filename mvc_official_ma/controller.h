#ifndef CONTROLLER_H
#define CONTROLLER_H

#include "data.h"

class Widget;

class Controller
{
public:
    Controller(Widget *newGui);
    void moveToken(int tokenNr, int newPosLogic);

    void newToken(int tokenNr, QString newName);
    int getPosToken(int tokenNr);
private:
    Widget *gui;
    Data *data;
    Data *datas2[20];
};

#endif // CONTROLLER_H
