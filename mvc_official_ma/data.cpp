#include "data.h"

/**
 * @brief Data::Data
 *
 * alle Token auf dummy "leer/nicht vorhanden"
 */
Data::Data()
{
    token0 = nullptr;
    token1 = nullptr;
    token2 = nullptr;
    token3 = nullptr;
}

/**
 * @brief neue Token anlegen
 * @param tokenNr Nummer des Steins
 * @param newName Name des Spielers
 */
void Data::newToken(int tokenNr, QString newName)
{
    switch (tokenNr) {
    case 0:
        token0 = new Token(newName);
        break;
    case 1:
        token1 = new Token(newName);
        break;
    case 2:
        token2 = new Token(newName);
        break;
    case 3:
        token3 = new Token(newName);
        break;
    default:
        break;
    }
}

int Data::getPosToken(int tokenNr)
{
    int retValue = -1;
    switch (tokenNr) {
    case 0:
        if(nullptr != token0)
            retValue = token0->getPosLogic();
        break;
    case 1:
        token1->getPosLogic();
        break;
    case 2:
        token2->getPosLogic();
        break;
    case 3:
        token3->getPosLogic();
        break;
    default:
        break;
    }
    return retValue;
}

/**
 * @brief Stein aktualisieren
 * @param tokenNr Nummer des Steins
 * @param newPos neue Position
 */
void Data::updateToken(int tokenNr, int newPos)
{
    switch (tokenNr) {
    case 0:
        if(nullptr != token0)
            token0->setPosLogic(newPos);
        break;
    case 1:
        token1->setPosLogic(newPos);
        break;
    case 2:
        token2->setPosLogic(newPos);
        break;
    case 3:
        token3->setPosLogic(newPos);
        break;
    default:
        break;
    }
}
