#ifndef DATA_H
#define DATA_H

#include <QString>
#include "token.h"

class Data
{
public:
    Data();
    void updateToken(int tokenNr, int newPos);
    void newToken(int tokenNr, QString newName);
    int getPosToken(int tokenNr);
private:
    Token *token0;
    Token *token1;
    Token *token2;
    Token *token3;
};

#endif // DATA_H
