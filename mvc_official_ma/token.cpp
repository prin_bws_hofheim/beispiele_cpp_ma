#include "token.h"

Token::Token(QString newName)
{
    name = newName;
}

int Token::getPosLogic()
{
    return posLogic;
}

QString Token::getName()
{
    return name;
}

void Token::setPosLogic(int newPosLogic)
{
    posLogic = newPosLogic;
}
