#ifndef TOKEN_H
#define TOKEN_H

#include <QString>

class Token
{
public:
    Token(QString newName);
    int getPosLogic();
    QString getName();
    void setPosLogic(int newPosLogic);

private:
    QString name;
    int posLogic;
};

#endif // TOKEN_H
