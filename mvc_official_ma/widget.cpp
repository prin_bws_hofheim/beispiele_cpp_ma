#include "widget.h"
#include "ui_widget.h"

Widget::Widget(QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::Widget)
{
    ui->setupUi(this);
    control = new Controller(this);

    QObject::connect(ui->btnMoveToken0, &QPushButton::clicked, this, &Widget::updateToken0Pos);
    QObject::connect(ui->btnStartGame, &QPushButton::clicked, this, &Widget::startGame);
    QObject::connect(ui->btnGetToken0Pos, &QPushButton::clicked, this, &Widget::getToken0Pos);
}

Widget::~Widget()
{
    delete ui;
}

/**
 * @brief Widget::startGame
 *
 * Token anlegen
 */
void Widget::startGame()
{
    for (int tokenNr = 0; tokenNr < 4; ++tokenNr) {
        control->newToken(tokenNr, "token"+QString::number(tokenNr));
    }
}

/**
 * @brief Widget::updateToken0Pos
 *
 * Pos von Token ändern
 */
void Widget::updateToken0Pos()
{
    control->moveToken(0, ui->sbToken0NewPos->value());
}

/**
 * @brief Widget::getToken0Pos
 * @param tokenNr Nr des Steins
 *
 * holt die aktuelle Pos des Steins und zeigt sie an
 * @bug Anzeige des label ist zu kurz, zeigt nur eine Stelle der Pos an. Log funtioniert.
 */
void Widget::getToken0Pos(int tokenNr)
{
    int tokenPos = -1;
    QString outString;
    tokenPos = control->getPosToken(tokenNr);
    outString = "Stein 0: " + QString::number(tokenPos);
    ui->lblToken0Pos->setText(outString);
    logAction("Widget", outString);
}

void Widget::Main()
{
    if(nullptr != control)
        control->getPosToken(0);
}

/**
 * @brief Ausgabe an log (Widget/TextEdit)
 * @param sender Objekt, das logt
 * @param logText Text für Anzeige
 */
void Widget::logAction(QString sender, QString logText)
{
    ui->teLog->appendPlainText(sender + ": " + logText);
}
