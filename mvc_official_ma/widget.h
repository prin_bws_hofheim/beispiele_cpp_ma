#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>
#include "controller.h"

QT_BEGIN_NAMESPACE
namespace Ui { class Widget; }
QT_END_NAMESPACE

class Widget : public QWidget
{
    Q_OBJECT

public:
    Widget(QWidget *parent = nullptr);
    ~Widget();
public slots:
    void logAction(QString sender, QString logText);

private slots:
    void updateToken0Pos();
    void startGame();
    void getToken0Pos(int tokenNr);
private:
    Ui::Widget *ui;
    Controller *control;
    void Main();
};
#endif // WIDGET_H
