#include "patch.h"

Patch::Patch()
{
    patchId = -1;
    field =  -1;
    quality = 0;
    status = statusUnknown;
}

Patch::Patch(int newPatchId, int newField, unsigned short newQuality, statusPatch newStatus)
{
    patchId = newPatchId;
    field =  newField;
    quality = newQuality;
    status = newStatus;
}

int Patch::getPatchId()
{
    return patchId;
}

void Patch::setPatchId(int newPatchId)
{
    patchId = newPatchId;
}

int Patch::getField()
{
    return field;
}

void Patch::setField(int newfield)
{
    field = newfield;
}

unsigned short Patch::getQuality()
{
    return quality;
}

void Patch::setQuality(unsigned short newquality)
{
    quality = newquality;
}

Patch::statusPatch Patch::getStatus()
{
    return status;
}

void Patch::setStatus(statusPatch newStatus)
{
    status = newStatus;
}
