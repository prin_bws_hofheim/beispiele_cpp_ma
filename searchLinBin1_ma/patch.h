#ifndef PATCH_H
#define PATCH_H


class Patch
{
public:
    enum statusPatch {statusFree, statusRequested, statusLeased, statusUnknown = -1};
    Patch();
    Patch(int newPatchId, int newField, unsigned short newQuality, statusPatch newStatus);
    int getPatchId();
    void setPatchId(int newPatchId);

    int getField();
    void setField(int newField);

    unsigned short getQuality();
    void setQuality(unsigned short newQuality);

    statusPatch getStatus();
    void setStatus(statusPatch newStatus);

private:
    int patchId;
    int field;
    unsigned short quality;
    statusPatch status;
};

#endif // PATCH_H
