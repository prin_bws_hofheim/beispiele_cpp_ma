#include "widget.h"
#include "ui_widget.h"

Widget::Widget(QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::Widget)
{
    ui->setupUi(this);
    connect(ui->btnFindLin, &QPushButton::clicked, this, &Widget::findLin);
    connect(ui->btnFindBin, &QPushButton::clicked, this, &Widget::findBin);
    connect(ui->btnShuffle, &QPushButton::clicked, this, &Widget::shuffle);
}

Widget::~Widget()
{
    delete ui;
}

void Widget::findLin()
{
    int patchId = -1;
    int fieldId = -1;

    patchId = ui->spinBox->value();

    if(ug21.findLinPatch(patchId, fieldId))
        ui->plainTextEdit->appendPlainText("gefunden");
    else
        ui->plainTextEdit->appendPlainText("nicht gefunden");
}

void Widget::findBin()
{
    int patchId = -1;
    int fieldId = -1;

    patchId = ui->spinBox->value();

    if(ug21.findBinPatch(patchId, fieldId))
        ui->plainTextEdit->appendPlainText("gefunden");
    else
        ui->plainTextEdit->appendPlainText("nicht gefunden");
}

void Widget::shuffle()
{

}

