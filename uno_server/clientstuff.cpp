#include "clientstuff.h"

ClientStuff::ClientStuff(QTcpSocket *newTcpSocket)
{
    tcpSocket = newTcpSocket;

}


/**
 * @brief ClientStuff::recData
 *
 * Befehl;Daten
 *
 * Befehl: Aktion/Inhalt
 * 11: Name des Users
 * 12: disconnect User
 * 21: Karte
 */
void ClientStuff::recData()
{
    QString recString;
    QStringList recStringList;
    int recCommand = -1;

    while (tcpSocket->bytesAvailable()) {
        recString = tcpSocket->readLine();
        emit logText(recString);
        recStringList = recString.split(";");

        if(recStringList.size()>0 && recStringList.size()<2){
            recCommand = recStringList[0].toInt();
            computeRecData(recCommand);
        }
        if(recStringList.size()>=2){
            recCommand = recStringList[0].toInt();
            computeRecData(recCommand, recStringList[1]);
        }

    }
}

void ClientStuff::sendString(QString sendString)
{
    tcpSocket->write(sendString.toLocal8Bit());
    qDebug() << "send: " << sendString;
}

void ClientStuff::computeRecData(int recCommand)
{
    switch (recCommand) {
    case CommandNumber::commandUsername:

        break;
    default:
        break;
    }
}


void ClientStuff::computeRecData(int recCommand, QString recParam)
{
    switch (recCommand) {
    case CommandNumber::commandUsername:

        break;
    default:
        break;
    }
}

void ClientStuff::sendData(int command)
{
    tcpSocket->write(QString::number(command).toLocal8Bit());
}

void ClientStuff::sendData(int command, QString param)
{
    QString sendString;
    sendString = QString::number(command) + ";" + param;
    tcpSocket->write(QString::number(command).toLocal8Bit());


}
