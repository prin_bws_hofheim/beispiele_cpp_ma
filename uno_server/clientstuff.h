#ifndef CLIENTSTUFF_H
#define CLIENTSTUFF_H

#include <QObject>
#include <QTcpSocket>
#include "../uno_server/commandnumber.h"

class ClientStuff : public QObject
{
    Q_OBJECT
public:
    ClientStuff(QTcpSocket *newTcpSocket);
    void sendData(int command);
    void sendData(int command, QString param);
public slots:
    void recData();
    void sendString(QString sendString);
signals:
    void logText(QString text);
private slots:

    void computeRecData(int recCommand);
    void computeRecData(int recCommand, QString recParam);

private:
    QTcpSocket *tcpSocket;
};

#endif // CLIENTSTUFF_H
