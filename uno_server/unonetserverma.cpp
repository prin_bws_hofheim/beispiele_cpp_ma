#include "unonetserverma.h"

UnoNetServerMa::UnoNetServerMa(QObject* parent) : QObject(parent)
{
    tcpServer = new QTcpServer;
    QObject::connect(tcpServer, &QTcpServer::newConnection, this, &UnoNetServerMa::newConnect);
    nextId = 0;
}

void UnoNetServerMa::startServer(unsigned short portNr)
{
    tcpServer->listen(QHostAddress::Any, portNr);
    emit logText("Server is open");
}

void UnoNetServerMa::stopServer()
{
    tcpServer->close();
    emit logText("Server is closed");
}

bool UnoNetServerMa::serverIsListening()
{
    return tcpServer->isListening();
}

void UnoNetServerMa::sendToClient(QString sendString)
{
    foreach (ClientStuff* n, clients) {
        n->sendString(sendString);
    }
}

void UnoNetServerMa::newConnect()
{
    QTcpSocket *tempSocket = nullptr;
    ClientStuff *tempClient = nullptr;

    while (tcpServer->hasPendingConnections()) {
        tempSocket = tcpServer->nextPendingConnection();
        tempClient = new ClientStuff(tempSocket);
        QObject::connect(tempClient, &ClientStuff::logText, this, &UnoNetServerMa::logText);
        QObject::connect(tempSocket, &QTcpSocket::readyRead, tempClient, &ClientStuff::recData);
        clients.push_back(tempClient);
        emit logText("new client: " + QString::number(nextId));
        nextId++;
    }
}

