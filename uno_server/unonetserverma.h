#ifndef UNONETSERVERMA_H
#define UNONETSERVERMA_H

#include <QTcpServer>
#include <QTcpSocket>
#include <map>
#include "clientstuff.h"

using namespace std;
class UnoNetServerMa : public QObject
{
    Q_OBJECT
public:
    UnoNetServerMa(QObject* parent = nullptr);
    void startServer(unsigned short portNr);
    void stopServer();

    bool serverIsListening();
public slots:
    void sendToClient(QString sendString);
signals:
    void logText(QString text);
private slots:
    void newConnect();
private:
    vector<ClientStuff*> clients;

#endif // UNONETSERVERMA_H

    QTcpServer* tcpServer;
    int nextId;
};
