#include "widget.h"
#include "ui_widget.h"

Widget::Widget(QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::Widget)
{
    ui->setupUi(this);
    netServer = new UnoNetServerMa;

    QObject::connect(ui->btnStartStopServer, &QPushButton::clicked, this, &Widget::startStopSever);
    QObject::connect(netServer, &UnoNetServerMa::logText, this, &Widget::logToUi);
    QObject::connect(ui->btnSendString, &QPushButton::clicked, this, &Widget::sendToClient);
}

Widget::~Widget()
{
    delete ui;
}

void Widget::startStopSever()
{
    if(netServer->serverIsListening()){
        netServer->stopServer();
        ui->btnStartStopServer->setText("start server");
        ui->sbPort->setEnabled(true);
    }
    else{
        netServer->startServer(ui->sbPort->value());
        ui->btnStartStopServer->setText("stop server");
        ui->sbPort->setEnabled(false);
    }
}

void Widget::logToUi(QString logText)
{
    ui->teLog->append(logText);
}

void Widget::sendToClient()
{
    QString sendString;
    sendString = ui->leSendString->text();
    netServer->sendToClient(sendString);
}
